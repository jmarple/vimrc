sudo apt-get update
sudo apt-get upgrade
sudo apt-get -y install \
  nvim \
  ranger \
  tmux \
  zsh \
  python3 \
  python3-pip \
  build-essential \ #ycm
  cmake \ #ycm
  python3-dev 

ln -s ~/dotfiles/vimrc ~/.vimrc
mkdir ~/.config/nvim
ln -s ~/dotfiles/nvimrc ~/.config/nvim/init.vim

nvim +'PlugInstall --sync' +qa

# setup fonts
wget https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Inconsolata/complete/Inconsolata%20Nerd%20Font%20Complete.otf -P ~/.fonts 

# setup fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf\n~/.fzf/install

# install YCM (separate steps in case one fails)
python3 ~/.vim/plugged/YouCompleteMe/install.py --all 
python3 ~/.vim/plugged/YouCompleteMe/install.py --java-completer
python3 ~/.vim/plugged/YouCompleteMe/install.py --ts-completer 
