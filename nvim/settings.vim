autocmd BufNewFile,BufEnter * silent! lcd %:p:h

let mapleader = " "
nnoremap ;; ;
nnoremap <leader>; :w<CR>
nnoremap ; :
set rnu
filetype plugin indent on
" On pressing tab, insert 2 spaces
set expandtab
" show existing tab with 2 spaces width
set tabstop=2
set softtabstop=2
" when indenting with '>', use 2 spaces width
set shiftwidth=2
set autoindent

let g:netrw_banner=0
	
" Ignore case when searching
set ignorecase

" When searching try to be smart about cases 
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch 

" Don't redraw while executing macros (good performance config)
set lazyredraw 

" Set the working directory to current file
set autochdir

set timeoutlen=1000
set ttimeoutlen=5

